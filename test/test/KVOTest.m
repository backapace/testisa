//
//  KVOTest.m
//  test
//
//  Created by 欧小强 on 2022/10/16.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "KVOTest.h"

@interface Animal : NSObject
@property (nonatomic, readonly) NSString *fullName;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@end
@implementation Animal

- (NSString *)fullName {
    return [NSString stringWithFormat:@"%@ %@", _firstName, _lastName];
}

// 使得fullName的监听和firstName、lastName相关联，fullName这个属性依赖于firstName和lastName,
// 修改firstName或者lastName都会触发通知监听key为fullName的观察者
+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    if ([key isEqualToString:@"fullName"]) {
        NSArray *affectingKeys = @[@"lastName", @"firstName"];
        keyPaths = [keyPaths setByAddingObjectsFromArray:affectingKeys];
    }
    return keyPaths;
}

@end

@interface KVOTest()
@property(nonatomic, strong) Animal *animal;
@end

@implementation KVOTest

- (id)init {
    self = [super init];
    if (self) {
        _animal = [Animal new];
    }
    return self;
}

- (void)changeFirstName:(NSString *)firstName {
    _animal.firstName = firstName;
}

- (void)changeLastName:(NSString *)lastName {
    _animal.lastName = lastName;
}

- (NSString *)getFullName {
    return _animal.fullName;
}

- (int)b {
    return (self.a + 1);
}

+ (NSSet *)keyPathsForValuesAffectingB {
    return [NSSet setWithObjects:@"a", nil];
}

@end
