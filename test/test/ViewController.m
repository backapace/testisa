//
//  ViewController.m
//  test
//
//  Created by 欧小强 on 2020/4/28.
//  Copyright © 2020 APPL. All rights reserved.
//

#import "ViewController.h"
#import <objc/runtime.h>
#import "KVOTest.h"
#import "KeyedArchiverTest.h"

@implementation Ktb

@end

@interface ViewController ()
@property (nonatomic, strong) KVOTest *kvoObj;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // [self testMetaPath];
    [self startObserver];
    [self addBtn];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"%s %p", __func__, self);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%s %p", __func__, self);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"%s %p", __func__, self);
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"%s %p", __func__, self);
}

- (void)addBtn {
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 50, 80)];
    btn.backgroundColor = [UIColor redColor];
    [btn setTitle:@"presentVC" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(presentVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(100, 200, 50, 80)];
    btn1.backgroundColor = [UIColor yellowColor];
    [btn1 setTitle:@"dismissVC" forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(dismissVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
}

- (void)presentVC:(UIButton *)btn {
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController:vc animated:true completion:nil];
}

- (void)dismissVC:(UIButton *)btn {
    [self dismissViewControllerAnimated:YES completion:^{
        nil;
    }];
}

- (void)testMetaPath {
    BOOL res0 = [[NSObject class] isKindOfClass:[NSObject class]];
    BOOL res1 = [[NSObject class] isMemberOfClass:[NSObject class]];
    BOOL res2 = [[NSObject class] isMemberOfClass:object_getClass([NSObject class])];
    BOOL res3 = [[UITableView class] isKindOfClass:object_getClass([UITableView class])];
    BOOL res4 = [[UITableView class] isMemberOfClass:object_getClass([UITableView class])];
    BOOL res5 = [[UITableView class] isKindOfClass:[UITableView class]];
    BOOL res6 = [[UITableView class] isMemberOfClass:[UITableView class]];
    NSLog(@"\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n", @(res0), @(res1), @(res2), @(res3), @(res4), @(res5), @(res6));
    
    Class cls = object_getClass([NSObject class]);//NSObject的元类
    Class cls1 = object_getClass(cls);//根元类的元类指向自身
    Class cls2 = object_getClass(cls1);//根元类的元类指向自身
    NSLog(@"》》》》》》》from [NSObject class]\n\
          %p,（[NSObject class]）,\n\
          %p,（[[NSObject class] class]）,\n\
          %p,（[[NSObject class] superclass]）,\n\
          %p,（[object_getClass([NSObject class]) superclass]）,指向了[NSObject class],\n\
          %p,（object_getClass([NSObject class]）,\n\
          %p,\n\
          %p",
          [NSObject class],
          [[NSObject class] class],
          [[NSObject class] superclass],
          [object_getClass([NSObject class]) superclass],
          cls,cls1,cls2);
    
    //metaClass
    Class metaCls0 = object_getClass([UITableView class]);//获取UITableView的类元类
    Class metaCls1 = object_getClass(metaCls0);//获取UITableView的类元类的元类，指向根元类
    Class metaCls2 = object_getClass(metaCls1);//根元类的元类指向自身
    Class metaCls3 = object_getClass(metaCls2);//根元类的元类指向自身
    NSLog(@"》》》》》》》from [UITableView class]\n\
          %p,%@（[UITableView class]）,\n\
          %p,%@,\n\
          %p,%@,\n\
          %p,%@,\n\
          %p,%@",
          [UITableView class],
          @(class_isMetaClass([UITableView class])),
          metaCls0,@(class_isMetaClass(metaCls0)),
          metaCls1,@(class_isMetaClass(metaCls1)),
          metaCls2,@(class_isMetaClass(metaCls2)),
          metaCls3,@(class_isMetaClass(metaCls3)));
    
    UITableView *tb = [UITableView new];
    Class metaCls000 = object_getClass(tb);//获取UITableView类实例对象所属的类
    Class metaCls001 = object_getClass(metaCls000);//获取类对象的类元类
    Class metaCls002 = object_getClass(metaCls001);//类元类的元类，指向根元类
    Class metaCls003 = object_getClass(metaCls002);//根元类的元类指向自身
    NSLog(@"》》》》》》》from [UITableView new]\n\
          %p,%@,（[UITableView class]）,\n\
          %p,（[tb class]）,\n\
          %p,（object_getClass([tb class])）,\n\
          %p,%@,（object_getClass(tb)）,\n\
          %p,%@,\n\
          %p,%@,\n\
          %p,%@",
          [UITableView class],@(class_isMetaClass([UITableView class])),
          [tb class],
          object_getClass([tb class]),
          metaCls000,@(class_isMetaClass(metaCls000)),
          metaCls001,@(class_isMetaClass(metaCls001)),
          metaCls002,@(class_isMetaClass(metaCls002)),
          metaCls003,@(class_isMetaClass(metaCls003)));
    
    Ktb *ktb = [Ktb new];
    Class metaCls00 = object_getClass(ktb);//获取某自定义类实例对象所属的类
    Class metaCls01 = object_getClass(metaCls00);//获取自定义类对象的类元类
    Class metaCls02 = object_getClass(metaCls01);//类元类的元类，指向根元类
    Class metaCls03 = object_getClass(metaCls02);//根元类的元类指向自身
    NSLog(@"》》》》》》》from UITableView.subClass\n\
          %p,%@,（[Ktb class]）,\n\
          %p,（[ktb class]）,\n\
          %p,（object_getClass([ktb class])）,\n\
          %p,%@,（object_getClass(ktb)）,\n\
          %p,%@,\n\
          %p,%@,\n\
          %p,%@",
          [Ktb class],@(class_isMetaClass([Ktb class])),
          [ktb class],
          object_getClass([ktb class]),
          metaCls00,@(class_isMetaClass(metaCls00)),
          metaCls01,@(class_isMetaClass(metaCls01)),
          metaCls02,@(class_isMetaClass(metaCls02)),
          metaCls03,@(class_isMetaClass(metaCls03)));
    
    NSLog(@"》》》》》》》compare\n\
          %p,（object_getClass(ktb)）,\n\
          %p,（[object_getClass(ktb) superclass]）,\n\
          %p,（[object_getClass([object_getClass(ktb)) superclass]）",
          metaCls00,
          [metaCls00 superclass],
          [metaCls01 superclass]);
    NSLog(@"》》》》》》》\n\
          %p,（[Ktb class]）,\n\
          %p,（[UITableView class]）,\n\
          %p,（object_getClass([UITableView class])）",
          [Ktb class],
          [UITableView class],
          object_getClass([UITableView class]));
    NSLog(@"%p", ktb.superclass);
    
    
    NSLog(@"\nobject_getClass(ktb)：%p，\n[Ktb class]：%p，\nobject_getClass([Ktb class])：%p，\nobject_getClass(object_getClass(ktb))：%p", object_getClass(ktb), [Ktb class], object_getClass([Ktb class]), object_getClass(object_getClass(ktb)));
    NSLog(@"\nobject_getClass(tb)：%p，\n[UITableView class]：%p，\nobject_getClass([UITableView class])：%p，\nobject_getClass(object_getClass(tb))：%p", object_getClass(tb), [UITableView class], object_getClass([UITableView class]), object_getClass(object_getClass(tb)));
    NSLog(@"\nobject_getClass(object_getClass(tb).superclass)：%p，\nobject_getClass(object_getClass(tb)).superclass：%p", object_getClass([object_getClass(tb) superclass]), [object_getClass(object_getClass(tb)) superclass]);
    NSLog(@"\nobject_getClass(object_getClass(ktb).superclass)：%p", object_getClass([object_getClass(ktb) superclass]));
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    self.kvoObj.a ++;
    [self.kvoObj changeFirstName:@"cate"];
    [self.kvoObj changeLastName:@"doge"];
    
    [KeyedArchiverTest test];
}

- (void)startObserver {
    self.kvoObj = [KVOTest new];
    self.kvoObj.a = 1;
//    [self.kvoObj addObserver:self forKeyPath:@"b" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionPrior|NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
    [self.kvoObj addObserver:self forKeyPath:@"animal.fullName" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionPrior|NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSLog(@"KeyPath:%@, change：%@", keyPath, change);
}

@end
