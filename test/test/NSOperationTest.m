//
//  NSOperationTest.m
//  test
//
//  Created by 欧小强 on 2022/10/17.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "NSOperationTest.h"

@implementation NSOperationTest

+ (void)test {
//    [NSOperationTest invocationOperation];
//    [NSOperationTest blockOperation];
//    [NSOperationTest addExecutionBlock];
//    [NSOperationTest customerOperation];
//    [NSOperationTest operationQueue];
//    [NSOperationTest operationQueue_addOperations];
//    [NSOperationTest operationQueue_addOperationWithBlock];
//    [NSOperationTest maxConcurrentOperationCount];
//    [NSOperationTest threadCommunication];
//    [NSOperationTest addDependency];
    [NSOperationTest addDependency_queuePriority];
}

+ (void)invocationOperation{
    dispatch_queue_t queue = dispatch_queue_create("", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        // NSInvocationOperation单独使用时，并没有开启新的线程，任务都是在当前线程中执行的。
        // 注意：这里的子线程，是dispatch_async开启的，如果去掉，会在主线程里执行。
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(operation) object:nil];
        [operation start];
    });
}

+ (void)operation{
    for (int i = 0; i < 5; i++) {
        [NSThread sleepForTimeInterval:2];
        NSLog(@"%d--%@",i,[NSThread currentThread]);
    }
}

+ (void)blockOperation {
    // NSBlockOperation单独使用时，并未开启新的线程，任务的执行都是在当前线程中执行的。
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 5; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"%d--%@",i,[NSThread currentThread]);
        }
    }];
    [operation start];
}

+ (void)addExecutionBlock {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"blockOperation--%@", [NSThread currentThread]);
        }
    }];
    // addExecutionBlock方法添加了组个多的任务后，开启新的线程，任务是并发执行的
    // 这些操作（包括 blockOperationWithBlock 中的操作）可能在不同的线程中并发执行，这是由系统决定的。
    [operation addExecutionBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"executionBlock1--%@", [NSThread currentThread]);
        }
    }];
    [operation addExecutionBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"executionBlock2--%@", [NSThread currentThread]);
        }
    }];
    [operation addExecutionBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"executionBlock3--%@", [NSThread currentThread]);
        }
    }];
    [operation addExecutionBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"executionBlock4--%@", [NSThread currentThread]);
        }
    }];
    [operation addExecutionBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"executionBlock5--%@", [NSThread currentThread]);
        }
    }];
    [operation start];
}

+ (void)customerOperation{
    CustomerOperation *operation = [[CustomerOperation alloc] init];
    [operation start];
}

+ (void)operationQueue {
    // 主队列：通过[NSOperationQueue mainQueue]方式获取，凡是添加到主队列中的任务都会放到主线程中执行。
//    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    // 通过[[NSOperationQueue alloc] init]方式创建一个队列，凡是添加到自定义队列中的任务会自动放到子线程中执行。
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];

    NSBlockOperation *operation1 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation1--%@", [NSThread currentThread]);
        }
    }];

    NSBlockOperation *operation2 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation2--%@", [NSThread currentThread]);
        }
    }];
    [queue addOperation:operation1];
    [queue addOperation:operation2];
}

// 如果任务很多时，一个个添加到队列不免有些麻烦，那么addOperations就起作用了。
+ (void)operationQueue_addOperations {
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
//    NSOperationQueue *queue = [NSOperationQueue mainQueue];

    NSBlockOperation *operation1 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation1--%@", [NSThread currentThread]);
        }
    }];

    NSBlockOperation *operation2 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation2--%@", [NSThread currentThread]);
        }
    }];
    
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(operation) object:nil];
    
    NSArray *operationList = @[operation1,operation2, invocationOperation];
    [queue addOperations:operationList waitUntilFinished:NO];// waitUntilFinished参数，如果传YES，则表示会等待队列里面的任务执行完成后才会往下执行，也就是会阻塞线程。
    NSLog(@"end");
}

// NSOperationQueue还提供了一个addOperationWithBlock方法可以将operation对象添加到NSOperationQueue中。
+ (void)operationQueue_addOperationWithBlock {
    // 开启了新的线程，任务是并发执行的。如果将queue换成是mainQueue，那么任务将会在主线程中同步执行。
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
//        NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [queue addOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation1--%@", [NSThread currentThread]);
        }
    }];
    [queue addOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation2--%@", [NSThread currentThread]);
        }
    }];
}

+ (void)maxConcurrentOperationCount {
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
//    queue.maxConcurrentOperationCount = 1;//串行队列
    queue.maxConcurrentOperationCount = 2;//并发队列
    // maxConcurrentOperationCount的值并不是表示并发执行的线程数量，而是在一个队列中能够同时执行的任务的数量。
    // 比如以下3个任务，分别在3个不同的线程里完成，但同一时间，最多只有2个任务在并发执行，见log信息。
    NSLog(@"maxCount=%ld", (long)queue.maxConcurrentOperationCount);
    NSBlockOperation *operation1 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation1--%@", [NSThread currentThread]);
        }
    }];

    NSBlockOperation *operation2 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation2--%@", [NSThread currentThread]);
        }
    }];
    NSBlockOperation *operation3 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"operation3--%@", [NSThread currentThread]);
        }
    }];
    NSArray *operationList = @[operation1, operation2, operation3];
    [queue addOperations:operationList waitUntilFinished:YES];

    NSLog(@"end");
}
/*
 2022-10-17 23:14:49.747086+0800 test[25327:4135349] operation1--<NSThread: 0x600002c52340>{number = 3, name = (null)}
 2022-10-17 23:14:49.747099+0800 test[25327:4135347] operation2--<NSThread: 0x600002c40100>{number = 4, name = (null)}
 2022-10-17 23:14:51.747776+0800 test[25327:4135349] operation1--<NSThread: 0x600002c52340>{number = 3, name = (null)}
 2022-10-17 23:14:51.747791+0800 test[25327:4135347] operation2--<NSThread: 0x600002c40100>{number = 4, name = (null)}
 2022-10-17 23:14:53.748634+0800 test[25327:4135345] operation3--<NSThread: 0x600002c04c40>{number = 5, name = (null)}
 2022-10-17 23:14:55.753078+0800 test[25327:4135345] operation3--<NSThread: 0x600002c04c40>{number = 5, name = (null)}
 */

// NSOperation线程间的通讯
+ (void)threadCommunication{
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 4; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"子线程--%@", [NSThread currentThread]);
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            for (int i = 0; i < 2; i++) {
                [NSThread sleepForTimeInterval:2];
                NSLog(@"主线程--%@", [NSThread currentThread]);
            }
        }];
        
    }];
    [queue addOperation:operation];
}

// 添加任务之间的依赖关系。所谓的依赖关系就是任务A需要等待任务B完成之后才能继续执行。
+ (void)addDependency {
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];

    NSBlockOperation *operation1 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"1---%@", [NSThread currentThread]);
        }
    }];
    NSBlockOperation *operation2 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"2---%@", [NSThread currentThread]);
        }
    }];
    NSBlockOperation *operation3 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"3---%@", [NSThread currentThread]);
        }
    }];

    // operation1依赖于operation2和operation3，则先执行operation2和operation3，然后执行operation1
    [operation1 addDependency:operation2];
    [operation1 addDependency:operation3];
    NSArray *opList = @[operation1,operation2,operation3];
    NSArray *dependencies = [operation1 dependencies];
    NSLog(@"dependencies-%@",dependencies);
    [queue addOperations:opList waitUntilFinished:YES];
    NSLog(@"end");
    // 运行结果可以看出operation2和operation3执行完成后才去执行的operation1。
}

// NSOperation的优先级queuePriority
// queuePriority属性只对同一个队列中的任务有效。
// queuePriority属性不能取代依赖关系。
// 对于进入准备就绪状态的任务优先级高的任务优先于优先级低的任务。
// 优先级高的任务不一定会先执行，因为已经进入准备就绪状态的任务即使是优先级低也会先执行。
// 默认是NSOperationQueuePriorityNormal。
+ (void)addDependency_queuePriority {
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];

    NSBlockOperation *operation1 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"1---%@", [NSThread currentThread]);
        }
    }];
    NSBlockOperation *operation2 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"2---%@", [NSThread currentThread]);
        }
    }];
    NSBlockOperation *operation3 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"3---%@", [NSThread currentThread]);
        }
    }];

    // operation1依赖于operation2和operation3，则先执行operation2和operation3，然后执行operation1
    [operation1 addDependency:operation2];
    [operation1 addDependency:operation3];
    operation1.queuePriority = NSOperationQueuePriorityVeryHigh;
    operation2.queuePriority = NSOperationQueuePriorityVeryLow;
    NSArray *opList = @[operation1,operation2,operation3];
    NSArray *dependencies = [operation1 dependencies];
    NSLog(@"dependencies-%@",dependencies);
    [queue addOperations:opList waitUntilFinished:YES];
    NSLog(@"end");
    // 将operation1的优先级设置为最高NSOperationQueuePriorityVeryHigh，operation1依然是最后执行的，那是因为operation1依赖于operation2和operation3，在operation2和operation3未执行完成之前，operation1一直是处于为就绪状态，即使优先级最高，也不会执行。
}

@end

@implementation CustomerOperation
- (void)main {
    if(!self.isCancelled){
        for (int i = 0; i < 4; i++) {
            [NSThread sleepForTimeInterval:2];
            // 自定义的Operation单独使用时，并没有开启新的线程，任务的执行是在当前的线程中执行的。
            NSLog(@"%d--%@",i,[NSThread currentThread]);
        }
    }
}

@end
