//
//  NSTimerTest.m
//  test
//
//  Created by 欧小强 on 2022/10/18.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "NSTimerTest.h"

@implementation NSTimerTest

// NSTimer的初始化方式有两种，分别是invocation和selector两种调用方式，这两种方式区别不大，但是selector的方式更加简便。
/*
+ (NSTimer *)timerWithTimeInterval:(NSTimeInterval)ti invocation:(NSInvocation *)invocation repeats:(BOOL)yesOrNo;
+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)ti invocation:(NSInvocation *)invocation repeats:(BOOL)yesOrNo;

+ (NSTimer *)timerWithTimeInterval:(NSTimeInterval)ti target:(id)aTarget selector:(SEL)aSelector userInfo:(nullable id)userInfo repeats:(BOOL)yesOrNo;
+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)ti target:(id)aTarget selector:(SEL)aSelector userInfo:(nullable id)userInfo repeats:(BOOL)yesOrNo;
 //*/

+ (void)test {
//    [NSTimerTest selectorType];
//    [NSTimerTest invocationType];
    [NSTimerTest scheduledTimer];
};

// selector方式
+ (void)selectorType {
    NSTimer *timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(timerTest) userInfo:nil repeats:YES];

    // NSTimer依赖于RunLoop运行
    
    // NSDefaultRunLoopMode模式，切换RunLoop模式，定时器停止工作.
    // [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    
    // UITrackingRunLoopMode模式，切换RunLoop模式，定时器停止工作.
    // [[NSRunLoop currentRunLoop] addTimer:timer forMode:UITrackingRunLoopMode];
    
    // common modes的模式,以下三种模式的组合模式 NSDefaultRunLoopMode & NSModalPanelRunLoopMode & NSEventTrackingRunLoopMode
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

+ (void)timerTest {
    NSLog(@"hello");
}

// invocation方式
+ (void)invocationType {
    // 获取到方法的签名
    NSMethodSignature *signature = [[self class] methodSignatureForSelector:@selector(timerTest)];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    invocation.target = self;
    invocation.selector = @selector(timerTest);

    NSTimer *timer = [NSTimer timerWithTimeInterval:2.0 invocation:invocation repeats:YES];
    [[NSRunLoop currentRunLoop]addTimer:timer forMode:NSRunLoopCommonModes];
}

// scheduledTimerWithTimeInterval方法
// scheduledTimerWithTimeInterval方法可以创建timer，这个方法和timerWithTimeInterval的区别就在于前者会默认的将timer添加到了RunLoop，并且currentRunLoop是NSDefaultRunLoopMode，而后者是需要开发者手动的将timer添加到RunLoop中。
+ (void)scheduledTimer {
    NSMethodSignature *signature = [[self class] methodSignatureForSelector:@selector(timerTest)];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    invocation.target = self;
    invocation.selector = @selector(timerTest);

    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2.0 invocation:invocation repeats:YES];
    // scheduledTimer会默认的将timer添加到了RunLoop
}

@end


// NSTimer的循环引用
// 不是所有的NSTimer都会产生循环引用。
/*
 1、repeats参数为NO的情况下，不会产生循环引用。
 2、iOS10后的新的API方法timerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats block:(void (^)(NSTimer *timer))block也不会产生循环引用，但是不要忘记了在合适的地方调用invalidate方法停止定时器的运行。
 */
// 要解决NSTimer的循环引用问题就需要打破NSTimer和target之间的循环条件，有如下几种方式：

// NSProxy的方式

@interface DSProxy : NSProxy
@property (weak, nonatomic) id target;
+ (instancetype)proxyWithTarget:(id)target;
@end

@implementation DSProxy

+ (instancetype)proxyWithTarget:(id)target {
    DSProxy* proxy = [[self class] alloc];
    proxy.target = target;
    return proxy;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel{
    return [self.target methodSignatureForSelector:sel];
}

- (void)forwardInvocation:(NSInvocation *)invocation{
    SEL sel = [invocation selector];
    if ([self.target respondsToSelector:sel]) {
        [invocation invokeWithTarget:self.target];
    }
}

@end

@interface ProxyTimer : NSObject
+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval target:(id)target selector:(SEL)selector userInfo:(nullable id)userInfo repeats:(BOOL)repeats;
@end

@implementation ProxyTimer

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval target:(id)target selector:(SEL)selector userInfo:(nullable id)userInfo repeats:(BOOL)repeats{
    NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:[DSProxy proxyWithTarget:target] selector:selector userInfo:userInfo repeats:repeats];
    return timer;
}

@end


// 封装NSTimer的方式
@interface DSTimer : NSObject

@property (nonatomic, weak) id target;
@property (nonatomic) SEL selector;

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval target:(id)target selector:(SEL)selector userInfo:(nullable id)userInfo repeats:(BOOL)repeats;

@end

@implementation DSTimer

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval target:(id)target selector:(SEL)selector userInfo:(nullable id)userInfo repeats:(BOOL)repeats {
    DSTimer *dsTimer = [[DSTimer alloc] init];
    dsTimer.target = target;
    dsTimer.selector = selector;
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:dsTimer selector:@selector(timered:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop]addTimer:timer forMode:NSRunLoopCommonModes];
    return timer;
}

- (void)timered:(NSTimer *)timer {
    if ([self.target respondsToSelector:self.selector]) {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.selector withObject:timer];
        #pragma clang diagnostic pop
    }
}

@end

// 同样是封装NSTimer的方式，增加通过block做回调的实现
@interface NSTimer (DSTimer)

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval repeats:(BOOL)repeats blockTimer:(void (^)(NSTimer *))block;

@end

@implementation NSTimer (DSTimer)

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval repeats:(BOOL)repeats blockTimer:(void (^)(NSTimer *))block {
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(timeredForBlock:) userInfo:[block copy] repeats:repeats];
    [[NSRunLoop currentRunLoop]addTimer:timer forMode:NSRunLoopCommonModes];
    return timer;
}

+ (void)timeredForBlock:(NSTimer *)timer {
    void (^ block)(NSTimer *timer)  = timer.userInfo;
    block(timer);
}

@end
