//
//  NSOperationTest.h
//  test
//
//  Created by 欧小强 on 2022/10/17.
//  Copyright © 2022 APPL. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSOperationTest : NSObject
+ (void)test;
@end

@interface CustomerOperation : NSOperation

@end

NS_ASSUME_NONNULL_END
