//
//  SceneDelegate.h
//  test
//
//  Created by 欧小强 on 2020/4/28.
//  Copyright © 2020 APPL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

