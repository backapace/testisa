//
//  GCDTimerTest.m
//  test
//
//  Created by 欧小强 on 2022/10/18.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "GCDTimerTest.h"

@implementation GCDTimerTest

@end


typedef enum : NSUInteger {
    Status_Running,
    Status_Pause,
    Status_Cancle,
} TimerStatus;

/*
 GCD定时器调用 dispatch_source_create方法创建一个source源，
 然后通过dispatch_source_set_timer方法设置定时器，
 dispatch_source_set_event_handler设置定时器任务，
 初创建的定时器是暂停的，需要调用dispatch_resume方法启动定时器，
 可以调用dispatch_suspend或者dispatch_source_cancel停止定时器。
 */

/*
 GCD定时器的注意事项：
 1、dispatch_resume和dispatch_suspend调用要成对出现。
 2、source在suspend状态下，如果直接设置source = nil或者重新创建source都会造成crash。正确的方式是在resume状态下调用dispatch_source_cancel(source)释放当前的source。
 3、dispatch_source_set_event_handler回调是一个block，在添加到source中后会被source强引用，所以在这里需要注意循环引用的问题。正确的方法是使用weak+strong或者提前调用dispatch_source_cancel取消timer。
 */
@interface GCDTimer : NSObject

@property (nonatomic, strong) dispatch_source_t gcdTimer;
@property (nonatomic, assign) TimerStatus currentStatus;

@end

@implementation GCDTimer

- (void)scheduledTimerWithTimeInterval:(NSTimeInterval)interval runNow:(BOOL)runNow afterTime:(NSTimeInterval)afterTime repeats:(BOOL)repeats queue:(dispatch_queue_t)queue block:(void (^)(void))block  {
    /** 创建定时器对象
    * para1: DISPATCH_SOURCE_TYPE_TIMER 为定时器类型
    * para2-3: 中间两个参数对定时器无用
    * para4: 最后为在什么调度队列中使用
    */
    self.gcdTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    /** 设置定时器
    * para2: 任务开始时间
    * para3: 任务的间隔
    * para4: 可接受的误差时间，设置0即不允许出现误差
    * Tips: 单位均为纳秒
    */
    dispatch_time_t when;
    if (runNow) {
        when = DISPATCH_TIME_NOW;
    } else {
        when = dispatch_walltime(NULL, (int64_t)(afterTime * NSEC_PER_SEC));
    }
    dispatch_source_set_timer(self.gcdTimer, dispatch_time(when, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, 0);
    // dispatch_source_set_event_handler 这个函数在执行完之后，block 会立马执行一遍，后面隔一定时间间隔再执行一次。而 NSTimer 第一次执行是到计时器触发之后。这也是和 NSTimer 之间的一个显著区别。?（待验证，是不是可以通过dispatch_source_set_timer的第二个参数设置开始时间来避免）https://www.jianshu.com/p/f6e8de6be14f
    dispatch_source_set_event_handler(self.gcdTimer, ^{
        if (!repeats) {
            dispatch_source_cancel(self.gcdTimer);
        }
        block();
    });
    dispatch_resume(self.gcdTimer);
    self.currentStatus = Status_Running;
}

- (void)pauseTimer {
    if (self.currentStatus == Status_Running && self.gcdTimer) {
        dispatch_suspend(self.gcdTimer);
        self.currentStatus = Status_Pause;
    }
}

- (void)resumeTimer {
    if (self.currentStatus == Status_Pause && self.gcdTimer) {
        dispatch_resume(self.gcdTimer);
        self.currentStatus = Status_Running;
    }
}

- (void)stopTimer {
    if (self.gcdTimer) {
        dispatch_source_cancel(self.gcdTimer);
        self.currentStatus = Status_Cancle;
        self.gcdTimer = nil;
    }
}


@end
