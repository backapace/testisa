//
//  DateFormatter.m
//  test
//
//  Created by 欧小强 on 2023/6/16.
//  Copyright © 2023 APPL. All rights reserved.
//

#import "DateFormatter.h"

@implementation DateFormatter

+ (void)dataFormat {
    /*
     2023-06-16T10:41:04.665+08:00
     2023-06-16T10:41:04.665Z
     2023-06-16T10:41:04
     2023-06-16 10:41:04
     20230616104104
     20230615214356
     
     时间戳的入参再格式化。后续再验证。
     */
    [DateFormatter dataFormatSetTimeZoneUTC:@"2023-06-16T10:41:04.665+08:00"];// 2023-06-16 02:41:04（有误。这时不需要设置输出格式的时区为0时区，才能得到期望值）
    [DateFormatter dataFormatSetTimeZoneUTC:@"2023-06-16T10:41:04.665Z"];// 2023-06-16 10:41:04
    [DateFormatter dataFormatSetTimeZoneUTC:@"2023-06-16T10:41:04.665"];
    [DateFormatter dataFormatSetTimeZoneUTC:@"2023-06-16T10:41:04"];
    [DateFormatter dataFormatSetTimeZoneUTC:@"2023-06-16 10:41:04"];
    [DateFormatter dataFormatSetTimeZoneUTC:@"20230616104104"];
    [DateFormatter dataFormatNotSetTimeZoneUTC:@"2023-06-16T10:41:04.665+08:00"];// 2023-06-16 10:41:04
    [DateFormatter dataFormatNotSetTimeZoneUTC:@"2023-06-16T10:41:04.665Z"];// 2023-06-16 18:41:04（有误。Z结尾，意味输入时间是0时区时间，若想在东八区输出入参的时间，则需要设置输出格式的时区为0时区，才能得到期望值）
    [DateFormatter dataFormatNotSetTimeZoneUTC:@"2023-06-16T10:41:04.665"];
    [DateFormatter dataFormatNotSetTimeZoneUTC:@"2023-06-16T10:41:04"];
    [DateFormatter dataFormatNotSetTimeZoneUTC:@"2023-06-16 10:41:04"];
    [DateFormatter dataFormatNotSetTimeZoneUTC:@"20230616104104"];// 2023-06-15 21:43:56
}

+ (NSString *)dataFormatSetTimeZoneUTC:(NSString *)dateString {
    if (dateString == nil || [dateString isEqualToString:@""]) {
        return @"";
    }
    // 实例化NSDateFormatter
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    // 设置日期格式
    if (dateString.length==14) {
        formatter.dateFormat = @"yyyyMMddHHmmss";
    } else if ([dateString containsString:@"T"]) {
        if (dateString.length==19) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
        } else if (dateString.length==23) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
        } else if (dateString.length==24) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        } else if (dateString.length==29) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        }
    } else if (dateString.length==19) {
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    }
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    // 要转换的日期字符串
    NSDate *date = [formatter dateFromString:dateString];
    if(!date){
        //yyyy-MM-ddTHH:mm:ss.SSSZ   2023-04-12T22:47:27.901Z
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        // 要转换的日期字符串
        date = [formatter dateFromString:dateString];
    }
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *retult = [formatter stringFromDate:date];
    //NSLog(@"%s ori：%@  dateStr：%@", __func__, dateString, retult);
    NSLog(@"%s 1：%@", __func__, dateString);
    NSLog(@"%s 2：%@", __func__, retult);
    return retult;
}

+ (NSString *)dataFormatNotSetTimeZoneUTC:(NSString *)dateString {
    if (dateString == nil || [dateString isEqualToString:@""]) {
        return @"";
    }
    // 实例化NSDateFormatter
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    // 设置日期格式
    if (dateString.length==14) {
        formatter.dateFormat = @"yyyyMMddHHmmss";
    } else if ([dateString containsString:@"T"]) {
        if (dateString.length==19) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
        } else if (dateString.length==23) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
        } else if (dateString.length==24) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        } else if (dateString.length==29) {
            formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        }
    } else if (dateString.length==19) {
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    }
    
    // 要转换的日期字符串
    NSDate *date = [formatter dateFromString:dateString];
    if(!date){
        //yyyy-MM-ddTHH:mm:ss.SSSZ   2023-04-12T22:47:27.901Z
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        // 要转换的日期字符串
        date = [formatter dateFromString:dateString];
    }
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *retult = [formatter stringFromDate:date];
    //NSLog(@"%s ori：%@  dateStr：%@", __func__, dateString, retult);
    NSLog(@"%s 1：%@", __func__, dateString);
    NSLog(@"%s 2：%@", __func__, retult);
    return retult;
}

@end
