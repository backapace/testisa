//
//  DateFormatter.h
//  test
//
//  Created by 欧小强 on 2023/6/16.
//  Copyright © 2023 APPL. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DateFormatter : NSObject

+ (void)dataFormat;

@end

NS_ASSUME_NONNULL_END
