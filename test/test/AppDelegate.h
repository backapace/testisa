//
//  AppDelegate.h
//  test
//
//  Created by 欧小强 on 2020/4/28.
//  Copyright © 2020 APPL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

@interface NSJSONSerialization(excludeNanJSONSerialize)

+ (nullable NSData *)newDataWithJSONObject:(id)obj options:(NSJSONWritingOptions)opt error:(NSError **)error;

@end

@interface MyObject : NSObject
@property(strong, nonatomic) NSString *text;

@end
