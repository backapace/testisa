//
//  Sort.c
//  test
//
//  Created by 欧小强 on 2023/6/14.
//  Copyright © 2023 APPL. All rights reserved.
//

#include "Sort.h"



#define N 4

int aaa(void) {
    int num[N+1], i;
    for(i = 1; i <= N; i++) num[i] = i;
    perm(num, 1);
    return 0;
}

void perm(int *num, int i) {
    int j, k, tmp;
    if (i<N) {
        for (j=i; j<N; j++) {
            tmp = num[j];
            for (k=j; k>i; k--) {
                num[k] = num[k-1];
            }
            num[i] = tmp;
            perm(num, i+1);
            
            for (k=i; k<j; k++) {
                num[k] = num[k+1];
            }
            num[j] = tmp;
        }
    } else {
        for (j=1; j<N; j++) {
            printf("%d ", num[j]);
        }
        printf("\n");
    }
}


