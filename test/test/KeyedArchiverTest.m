//
//  KeyedArchiverTest.m
//  test
//
//  Created by 欧小强 on 2022/10/20.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "KeyedArchiverTest.h"

static bool trigger = true;

@interface Person1 : NSObject<NSCoding>
//需要归档的属性
@property (nonatomic, strong)NSString       *name;
@property (nonatomic, assign)NSInteger      age;

@end

@implementation Person1

- (void)encodeWithCoder:(NSCoder *)coder {
    //告诉系统归档的属性是哪些
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeInteger:self.age forKey:@"age"];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        //解档
        self.name = [coder decodeObjectForKey:@"name"];
        self.age = [coder decodeIntegerForKey:@"age"];
    }
    return self;
}

@end

@implementation KeyedArchiverTest

+ (void)test {
    if (trigger) {
        [KeyedArchiverTest save];
        trigger = false;
    } else {
        [KeyedArchiverTest read];
        trigger = true;
    }
}

+ (void)save {
    Person1 *person = [[Person1 alloc] init];
    person.name = @"Frank";
    person.age = 18;
    
    //这里以temp路径为例，存到temp路径下
    NSString *temp = NSTemporaryDirectory();
    NSString *filePath = [temp stringByAppendingPathComponent:@"obj.data"]; //注：保存文件的扩展名可以任意取，不影响。
    NSLog(@"%@", filePath);
    //归档
    [NSKeyedArchiver archiveRootObject:person toFile:filePath];
}

+ (void)read {
    //取出归档的文件再解档
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"obj.data"];
    //解档
    Person1 *person = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    NSLog(@"name = %@, age = %ld",person.name,person.age);
}

@end
