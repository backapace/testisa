//
//  AttrConstructor.m
//  test
//
//  Created by 欧小强 on 2022/10/8.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "AttrConstructor.h"

//#include <objc/runtime.h>
__attribute__((constructor)) void load_file() {
    printf("Consturctor is called.\n");
}

@implementation AttrConstructor

+ (void)load {
    NSLog(@"%s", __func__);
}

@end
