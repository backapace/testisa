//
//  MethodExchangeTest.m
//  test
//
//  Created by 欧小强 on 2022/10/6.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "MethodExchangeTest.h"

@implementation MethodExchangeTest

+ (void)test {
    MethodEx *methodEx = [[MethodEx alloc] init];
    NSLog(@"交换前");
    [methodEx test3];
    NSLog(@"--------------");
    NSLog(@"交换后");
    [methodEx exchangeBase];
    [methodEx test3];
    NSLog(@"--------------");
    NSLog(@"新的子类实例");
    MethodEx *methodEx1 = [[MethodEx alloc] init];
    [methodEx1 test3];
    NSLog(@"调用 子类 test1：");
    [methodEx1 test1];
    
    NSLog(@"--------------");
    NSLog(@"父类实例");
    MethodBase *base = [[MethodBase alloc] init];
    [base test3];
}

@end


#import <objc/runtime.h>
@implementation MethodBase
- (void)test3{
    [self test11];
    NSLog(@"test3");
}

- (void)exchangeBase{
    SEL test1 = @selector(test1);
    SEL test11 = @selector(test11);
    SEL test3 = @selector(test3);

    Method method1 = class_getInstanceMethod([self class], test1);
    Method method11 = class_getInstanceMethod([self class], test11);
    Method method3 = class_getInstanceMethod([self class], test3);
    
    
//    method_exchangeImplementations(method11, method1);

    if(class_addMethod([self class], test11, method_getImplementation(method1), "v@:"))
    {
        class_replaceMethod([self class], test1, method_getImplementation(method11), "v@:");
    }else{
        method_exchangeImplementations(method11, method1);
    }
    
}

-(void)test11{
    NSLog(@"test11");
}
@end

@implementation MethodEx
- (void)test1{
    NSLog(@"test1");
}
@end
