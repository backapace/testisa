//
//  GCDTest.m
//  test
//
//  Created by 欧小强 on 2022/10/16.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "GCDTest.h"

@implementation GCDTest

+ (void)test {
    [GCDTest testTaskAndQueue];
    [GCDTest testBarrierTask];
    [GCDTest testAfterTask];
    [GCDTest shareInstance];// dispatch_once
    [GCDTest testGroupNotify];
    [GCDTest testGroupEnter];
    [GCDTest testSemaphore];
}

+ (void)testTaskAndQueue {
    
    //『串行队列』中的任意类型任务（无论是异步或同步），里边嵌套『同一串行队列』的『同步任务』，会造成死锁。
    // 原因：串行队列要求任务顺序执行的特性，与同步任务会阻塞线程的特性，会造成执行顺序的冲突
    /*
    {
        //『同一串行队列』，异步任务嵌同步任务的情况
        dispatch_queue_t queue = dispatch_queue_create("com.shen.thread.demo", DISPATCH_QUEUE_SERIAL);
        //异步任务A
        dispatch_async(queue, ^{
            //同步任务B
            dispatch_sync(queue, ^{
                NSLog(@"任务C---%@",[NSThread currentThread]);
            });
            NSLog(@"任务B---%@",[NSThread currentThread]);
        });
    }
     //*/
    
    /*
    {
        //『同一串行队列』，同步任务嵌同步任务的情况
        dispatch_queue_t queue = dispatch_queue_create("com.shen.thread.demo", DISPATCH_QUEUE_SERIAL);
        //同步任务A
        dispatch_sync(queue, ^{
            //同步任务B
            dispatch_sync(queue, ^{
                NSLog(@"任务C---%@",[NSThread currentThread]);
            });
            NSLog(@"任务B---%@",[NSThread currentThread]);
        });
    }
     //*/
    
    /*
    {
        // 特殊地，以下代码通常会造成死锁，除非本方法testTaskAndQueue是在非主线程队列里，否则道理同上。
        dispatch_queue_t queue = dispatch_get_main_queue();
        dispatch_sync(queue, ^{
            NSLog(@"任务：%@", [NSThread currentThread]);
        });
    }
     //*/
    
    /*
    {
        dispatch_queue_t q1 = dispatch_queue_create(@"test", DISPATCH_QUEUE_SERIAL);
        dispatch_queue_t q2 = dispatch_queue_create(@"test", DISPATCH_QUEUE_SERIAL);
        NSLog(@"q1: %p", q1);
        NSLog(@"q2: %p", q2);
        NSLog(@"mainqueue: %p", [NSThread mainThread]);
        NSLog(@"before: %p", [NSThread currentThread]);
        dispatch_async(q1, ^{
            NSLog(@"t1: %p", [NSThread currentThread]);
            dispatch_sync(q2, ^{
                NSLog(@"t2: %p", [NSThread currentThread]);
            });
            NSLog(@"end async");
        });
    }
     //*/
    
    
}

+ (void)testBarrierTask {
    /*
    {
        dispatch_queue_t queue = dispatch_queue_create("com.shen.thread.demo", DISPATCH_QUEUE_CONCURRENT);
        NSLog(@"start");
        dispatch_async(queue, ^{
            NSLog(@"currentThread-1:%@", [NSThread currentThread]);
        });
        dispatch_barrier_async(queue, ^{
            NSLog(@"currentThread-2:%@", [NSThread currentThread]);
            [NSThread sleepForTimeInterval:2];
        });
        // 若将dispatch_barrier_async换成dispatch_barrier_sync，则会阻塞当前线程，pause会等2秒后再打印。
        NSLog(@"pause");
        dispatch_async(queue, ^{
            NSLog(@"currentThread-3:%@", [NSThread currentThread]);
        });
        NSLog(@"end");
    }
     //*/
}

+ (void)testAfterTask{
    /*
     // dispatch_after 方法并不是在指定时间之后才开始执行处理，而是在指定时间之后将任务追加到主队列中。
    NSLog(@"begin");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0*NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"after---%@",[NSThread currentThread]);
    });
     //*/
}

+ (instancetype)shareInstance {
    // dispatch_once方法可以保证一段代码在程序运行过程中只被调用一次，而且在多线程环境下可以保证线程安全。
    // static Test *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // instance = [Test alloc] init];
    });
    // return instance;
    return 0;
}

+ (void)testGroupNotify{
    // 调度组简单来说就是把异步执行的任务进行分组，等待所有的分组任务都执行完毕后再回到指定的线程执行任务。
    // 调用dispatch_group_notify方法回到指定线程执行任务，或者调用dispatch_group_wait阻塞当前线程。
    /*
    NSLog(@"current thread:%@", [NSThread currentThread]);
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
        [NSThread sleepForTimeInterval:2];
        NSLog(@"thread-1:%@", [NSThread currentThread]);
    });

    dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
        [NSThread sleepForTimeInterval:2];
        NSLog(@"thread-2:%@", [NSThread currentThread]);
    });

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [NSThread sleepForTimeInterval:2];
        NSLog(@"thread-3:%@", [NSThread currentThread]);
        NSLog(@"group-end");
    });

    // dispatch_group_wait阻塞当前线程，阻塞多久要结合第2个参数和dispatch_group_wait的机制而定
    // NSInteger res = dispatch_group_wait(group, dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC));
    // dispatch_group_wait(group, DISPATCH_TIME_NOW);
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    NSLog(@"dispatch_group_wait后继续执行任务");
     //*/
}

+ (void)testGroupEnter {
    // dispatch_group_enter和dispatch_group_leave捉对实现将队列添加到调度组的情况。
    // dispatch_group_enter 标志着在group的任务数+1，dispatch_group_leave 标志着在group中的任务数-1，表示已经完成了一个任务。
    /*
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
    dispatch_group_enter(group);

    dispatch_async(queue, ^{
        [NSThread sleepForTimeInterval:2];
        NSLog(@"thread_1:%@", [NSThread currentThread]);
        dispatch_group_leave(group);
    });

    dispatch_group_enter(group);
    dispatch_async(queue, ^{
        [NSThread sleepForTimeInterval:2];
        NSLog(@"thread_2:%@", [NSThread currentThread]);
        dispatch_group_leave(group);
    });

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [NSThread sleepForTimeInterval:2];
        NSLog(@"thread_3:%@", [NSThread currentThread]);
        NSLog(@"group_end");
    });
     //*/
}

+ (void)testSemaphore {
    // 创建一个 Semaphore 并初始化信号的总量
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    __block int a = 0;
    while (a < 5) {
        // 注意，如果这里放在主线程队列dispatch_get_main_queue()里，则会造成死循环。
        // 因为while在主线程里运行，而dispatch_async放异步任务到主队列里，需要等testSemaphore都运行完才会执行，while循环的结束条件 a < 5 永远不会成立，所以会一直循环。
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSLog(@"async里面的a的值：%d-----%@", a, [NSThread currentThread]);
            // 发送一个信号，让信号总量加 1
            dispatch_semaphore_signal(semaphore);
            a++;
        });
        // 可以使总信号量减 1，信号总量小于 0 时就会一直等待（阻塞所在线程），否则就可以正常执行。
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
        // 注释掉这里的dispatch_semaphore_wait，可以看到”有趣“的情况，a会出现超出预期的结果，这时恰恰可以体现多线程读写时，数据同步的重要性。这里的「读」是在主线程，而写是在非子线程里，信号量在数据同步时起到关键作用。
        NSLog(@"async外的a的值：%d-----%@", a, [NSThread currentThread]);
    }
    NSLog(@"外面的a的值：%d", a);
}

@end
