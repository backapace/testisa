//
//  AppDelegate.m
//  test
//
//  Created by 欧小强 on 2020/4/28.
//  Copyright © 2020 APPL. All rights reserved.
//

#import "AppDelegate.h"
#import <objc/runtime.h>
#import "MethodExchangeTest.h"
#import "KVCTest.h"
#import "GCDTest.h"
#import "NSOperationTest.h"
#import "NSTimerTest.h"
#import "StaticTest.h"
#import "KeyedArchiverTest.h"
//#include <stdio.h>
//#include <stdlib.h>
#include "Sort.h"
#import "DateFormatter.h"

#define TLog(prefix,Obj) {NSLog(@"变量内存地址：%p, 变量值：%p, 指向对象值：%@, --> %@",&Obj,Obj,Obj,prefix);}

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    aaa();
    [DateFormatter dataFormat];
    /*
     [KeyedArchiverTest test];
     //*/
    
    /*
    [StaticTest test];
     //*/
    
    /*
    [NSTimerTest test];
     //*/
    
    /*
    {
        [NSOperationTest test];
    }
     //*/
    
    //*
    {
        [GCDTest test];
    }
     //*/
    
    /*
    {
        [self testJSONSerializatio];
    }
     //*/
    
    //block变量读写测试
    //*
    {
        [self testBlock];
    }
     //*/

    //weak避免循环引用的验证
    /*
    {
        [self testWeakObjInBlock];
    }
     //*/
    
    /*
    {
        [self testCopy];
    }
     //*/
    
    /*
    {
        [MethodExchangeTest test];
    }
     //*/
    
    /*
    {
        [self testKVC];
    }
     //*/
    
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}

+ (BOOL)hasNanInContainer:(id)objs {
    BOOL found = NO;
    if ([objs isKindOfClass:[NSArray class]]) {
        NSArray *ary = (NSArray *)objs;
        for (id value in ary) {
            if ([value isKindOfClass:[NSNumber class]] && isnan([value doubleValue])) {
                found = YES;
                //NSLog(@"%s %@", __func__, ary);
                break;
            } else if ([value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSArray class]]) {
                BOOL res = [[self class] hasNanInContainer:value];
                if (res) {
                    found = YES;
                    break;
                }
            }
        }
    }
    else if ([objs isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic = (NSDictionary *)objs;
        for (id value in dic.allValues) {
            if ([value isKindOfClass:[NSNumber class]] && isnan([value doubleValue])) {
                //NSLog(@"%s %@", __func__, value);
                found = YES;
                break;
            } else if ([value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSArray class]]) {
                BOOL res = [[self class] hasNanInContainer:value];
                if (res) {
                    found = YES;
                    break;
                }
            }
        }
    }
    return found;
}

+ (id)excludeNanInContainer:(id)objs {
    id result = nil;
    if ([objs isKindOfClass:[NSArray class]]) {
        NSMutableArray *mutObj = [objs mutableCopy];
        [mutObj enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSNumber class]] && isnan([obj doubleValue])) {
                [mutObj replaceObjectAtIndex:idx withObject:@(0)];
            }
            else if ([obj isKindOfClass:[NSDictionary class]] || [obj isKindOfClass:[NSArray class]]) {
                [mutObj replaceObjectAtIndex:idx withObject:[[self class] excludeNanInContainer:obj]];
            }
        }];
        result = mutObj;
    }
    else if ([objs isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *mutObj = [objs mutableCopy];
        [mutObj enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSNumber class]] && isnan([obj doubleValue])) {
                [mutObj setObject:@(0) forKey:key];
            }
            else if ([obj isKindOfClass:[NSDictionary class]] || [obj isKindOfClass:[NSArray class]]) {
                [mutObj setObject:[[self class] excludeNanInContainer:obj] forKey:key];
            }
        }];
        result = mutObj;
    }
    return result;
}

//NSJSONSerialization写带有NaN的dictionary情况
- (void)testJSONSerialization {
    //识别NaN的测试代码
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@(NAN) forKey:@"key"];
    NSNumber *t = dic[@"key"];
    BOOL b = [t isKindOfClass:[NSNumber class]];
    b = isnan(t.doubleValue);
    BOOL found = NO;
    for (id value in dic.allValues) {
        if ([value isKindOfClass:[NSNumber class]] && isnan([value doubleValue])) {
            NSLog(@"%s %@", __func__, value);
            found = YES;
            break;
        }
    }

    if ([AppDelegate hasNanInContainer:dic]) {
        NSLog(@"There is Nan in \n%@", dic);
        //return nil;
    }
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    
    //测试NaN在数组中时是否会出现崩溃
    /*
    [dic setObject:@[@(NAN)] forKey:@"key"];
    data = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
     //*/
}

- (void)testBlock {
    {//值被拷贝，改变不影响block的操作
        int outA = 8;
        int (^myPtr) (int) = ^(int a) {return outA+a;};
        // block 裡面可以讀同一個scope的outA的值
        outA = 5; // 在呼叫myPtr之前改變outA的值
        int result = myPtr(3); // result 的值還是 11並不是 8。
        NSLog(@"%@", @(result));//myPtr在其主體用到outA這個變數值的時候是做了一個copy的動作把outA的值copy下來
    }
    {//static变量，修改会影响block的操作，也可被block修改
        static int outA = 8;
        int (^myPtr) (int) = ^(int a) {return outA+a;};
        // block 裡面可以讀同一個scope的outA的值
        outA = 5; // 在呼叫myPtr之前改變outA的值
        int result = myPtr(3); // result 的值是 8，因為outA是個static 變數會直接反應其值
        NSLog(@"%@", @(result));
    }
    {//static变量，修改会影响block的操作，也可被block修改
        static int outA = 8;
        int (^myPtr) (int) = ^(int a) { outA= 5; return outA+a;};
        // block 裡面改變outA的值
        int result = myPtr(3); // result 的值是 8，因為outA是個static 變數會直接反應其值
        NSLog(@"%@", @(result));
    }
    {//指针拷贝，本身不可被block修改，但内容可以被修改
        NSMutableArray * mutableArray = [NSMutableArray arrayWithObjects:@"one",@"two",@"three",nil];
        int result = ^(int a) { [mutableArray removeLastObject];  return a*a;} (5);
        NSLog(@"test array %@", mutableArray);
    }
    {//block变量：可被block修改
        __block int num = 5;
        int (^myPtr) (int) = ^(int a) { return num++;};
        int (^myPtr2) (int) = ^(int a) { return num++;};
        int result = myPtr(0);
        result = myPtr2(0);//result的值是6，num的值是7
        NSLog(@"%@", @(result));
    }
    {
        MyObject *obj = [[MyObject alloc]init];
        obj.text = @"11111111111111";
        TLog(@"obj",obj);
         
        __block MyObject *blockObj = obj;
        obj = nil;
        TLog(@"blockObj -1",blockObj);
         
        void(^testBlock)() = ^(){
            TLog(@"blockObj - block1",blockObj);
            MyObject *obj2 = [[MyObject alloc]init];
            obj2.text = @"222222222222";
            TLog(@"obj2",obj2);
            blockObj = obj2;
            TLog(@"blockObj - block2",blockObj);
        };
        NSLog(@"%@",testBlock);
        TLog(@"blockObj -2",blockObj);
        testBlock();
        TLog(@"blockObj -3",blockObj);
    }
}

- (void)testWeakObjInBlock {
    /*
     从输出结果可以看到

     block 内的 weakObj 和外部的 weakObj 并不是同一个变量
     block 捕获了 weakObj 同时也是对 obj 进行了弱引用，当我在 block 外把 obj 释放了之后，block 内也读不到这个变量了
     当 obj 赋值 nil 时，block 内部的 weakObj 也为 nil 了，也就是说 obj 实际上是被释放了，可见 __weak 是可以避免循环引用问题的
     ————————————————
     版权声明：本文为CSDN博主「Lion__Chen」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
     原文链接：https://blog.csdn.net/iphonecat/java/article/details/51226790
     */
    MyObject *obj = [[MyObject alloc]init];
    obj.text = @"111111111111";
    TLog(@"obj", obj);
     
    __weak MyObject *weakObj = obj;
    TLog(@"weakObj", weakObj);
     
    void(^testBlock)(void) = ^(){
        TLog(@"weakObj - block", weakObj);
    };
    testBlock();
    obj = nil;
    testBlock();
}

- (void)testCopy {
    /*
      对于集合类的可变对象来说，深拷贝并非严格意义上的深复制，虽然新开辟了内存，但是对于存放在数组里面的元素来说仍然是浅拷贝。
      */
    NSMutableArray *ary = [NSMutableArray array];
    [ary addObject:@"a"];
    [ary addObject:@"b"];
    [ary addObject:@"c"];
    for (NSString *s in ary) {
        NSLog(@"%p", s);
    }
    NSArray *aryc1 = [ary copy];
    for (NSString *s in aryc1) {
        NSLog(@"%p", s);
    }
    NSMutableArray *aryc2 = [ary mutableCopy];
    for (NSString *s in aryc2) {
        NSLog(@"%p", s);
    }
}

- (void)testKVC {
    [KVCTest testKVC];
}
 
@end


@implementation NSJSONSerialization(excludeNanJSONSerialize)
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self changeClassMethodToClass:object_getClass((id)self) method:@selector(dataWithJSONObject:options:error:) fromClass:object_getClass((id)self) toNewMethod:@selector(newDataWithJSONObject:options:error:)];
        NSLog(@"%p", object_getClass((id)self));
        NSLog(@"%p", [NSJSONSerialization class]);
        NSLog(@"%p", self);
    });
}
    

+ (nullable NSData *)newDataWithJSONObject:(id)obj options:(NSJSONWritingOptions)opt error:(NSError **)error {
    id newObj = obj;
    if ([NSJSONSerialization reverseDic:obj]) {
        NSLog(@"%s %@", __func__, obj);
        newObj = [NSJSONSerialization excludeNanInContainer:obj];
        NSLog(@"%s %@", __func__, newObj);
    }
    
    return [NSJSONSerialization newDataWithJSONObject:newObj options:opt error:error];
}

+ (id)excludeNanInContainer:(id)objs {
    id result = nil;
    if ([objs isKindOfClass:[NSArray class]]) {
        NSMutableArray *mutObj = [objs mutableCopy];
        [mutObj enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSNumber class]] && isnan([obj doubleValue])) {
                [mutObj replaceObjectAtIndex:idx withObject:@(0)];
            }
            else if ([obj isKindOfClass:[NSDictionary class]] || [obj isKindOfClass:[NSArray class]]) {
                [mutObj replaceObjectAtIndex:idx withObject:[NSJSONSerialization excludeNanInContainer:obj]];
            }
        }];
        result = mutObj;
    }
    else if ([objs isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *mutObj = [objs mutableCopy];
        [mutObj enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSNumber class]] && isnan([obj doubleValue])) {
                [mutObj setObject:@(0) forKey:key];
            }
            else if ([obj isKindOfClass:[NSDictionary class]] || [obj isKindOfClass:[NSArray class]]) {
                [mutObj setObject:[NSJSONSerialization excludeNanInContainer:obj] forKey:key];
            }
        }];
        result = mutObj;
    }
    return result;
}

+ (BOOL)reverseDic:(id)objs {
    BOOL found = NO;
    if ([objs isKindOfClass:[NSArray class]]) {
        NSArray *ary = (NSArray *)objs;
        for (id value in ary) {
            if ([value isKindOfClass:[NSNumber class]] && isnan([value doubleValue])) {
                found = YES;
                //NSLog(@"%s %@", __func__, ary);
                break;
            }
            else if ([value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSArray class]]) {
                BOOL res = [NSJSONSerialization reverseDic:value];
                if (res) {
                    found = YES;
                    break;
                }
            }
        }
    }
    else if ([objs isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic = (NSDictionary *)objs;
        for (id value in dic.allValues) {
            if ([value isKindOfClass:[NSNumber class]] && isnan([value doubleValue])) {
                //NSLog(@"%s %@", __func__, value);
                found = YES;
                break;
            }
            else if ([value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSArray class]]) {
                BOOL res = [NSJSONSerialization reverseDic:value];
                if (res) {
                    found = YES;
                    break;
                }
            }
        }
    }
    return found;
}

+ (void)changeClassMethodToClass:(Class)origClass method:(SEL)origSEL fromClass:(Class)newClass toNewMethod:(SEL)newSEL
{
    //取原有方法
    Method origMethod = class_getClassMethod(origClass, origSEL);
    //取新的方法
    Method newMethod = class_getClassMethod(newClass, newSEL);
    
    //method_exchangeImplementations(origMethod, newMethod);
    //*
    //把旧的方法，改成新的实现
    if(class_addMethod(origClass, origSEL, method_getImplementation(newMethod), method_getTypeEncoding(newMethod)))
    {
        //成功，则把新的方法改成旧的实现
        class_replaceMethod(origClass, newSEL, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else
    {
        //失败，则表示原来有origSEL的实现，直接把新旧更换
        method_exchangeImplementations(origMethod, newMethod);
    }
     //*/
}

@end

@implementation MyObject


@end
