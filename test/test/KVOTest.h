//
//  KVOTest.h
//  test
//
//  Created by 欧小强 on 2022/10/16.
//  Copyright © 2022 APPL. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KVOTest : NSObject
@property (nonatomic, assign) int a;
@property (nonatomic, readonly) int b;

- (void)changeFirstName:(NSString *)firstName;
- (void)changeLastName:(NSString *)lastName;

@end

NS_ASSUME_NONNULL_END
