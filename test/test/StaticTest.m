//
//  StaticTest.m
//  test
//
//  Created by 欧小强 on 2022/10/19.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "StaticTest.h"

static int age = 10;

@interface PersonExt : NSObject
-(void)add;
+(void)reduce;
@end

@implementation PersonExt

- (void)add {
    age++;
    NSLog(@"Person内部:%@-%p--%d", self, &age, age);
}

+ (void)reduce {
    age--;
    NSLog(@"Person内部:%@-%p--%d", self, &age, age);
}
@end


@implementation PersonExt (DS)

- (void)ds_add {
    age++;
    NSLog(@"Person (DS)内部:%@-%p--%d", self, &age, age);
}

@end

@implementation StaticTest

+ (void)test {
    NSLog(@"vc:%p--%d", &age, age);
    age = 40;
    NSLog(@"vc:%p--%d", &age, age);
    [[PersonExt new] add];
    NSLog(@"vc:%p--%d", &age, age);
    [PersonExt reduce];
    NSLog(@"vc:%p--%d", &age, age);
    [[PersonExt new] ds_add];
}

@end
