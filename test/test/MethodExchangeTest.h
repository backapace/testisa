//
//  MethodExchangeTest.h
//  test
//
//  Created by 欧小强 on 2022/10/6.
//  Copyright © 2022 APPL. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MethodExchangeTest : NSObject

+ (void)test;

@end

@interface MethodBase : NSObject
-(void)test3;
-(void)exchangeBase;
@end

@interface MethodEx : MethodBase
- (void)test1;
@end

NS_ASSUME_NONNULL_END
