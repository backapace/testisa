//
//  KVCTest.m
//  test
//
//  Created by 欧小强 on 2022/10/14.
//  Copyright © 2022 APPL. All rights reserved.
//

#import "KVCTest.h"
#import <UIKit/UIKit.h>

@interface Student : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSArray *classArr;
@end
@implementation Student
@end


typedef struct {
    float x, y, z;
} ThreeFloats;


@interface Person : NSObject {
    Student *_student;
}
@property (nonatomic, assign) NSInteger age;
@property (nonatomic) ThreeFloats threeFloats;
@end
@implementation Person
- (NSString *)description {
    return [NSString stringWithFormat:@"addr: %p, value: %@", _student.name, _student.name];;
}

- (BOOL)validateValue:(inout id _Nullable __autoreleasing *)ioValue forKey:(NSString *)inKey error:(out NSError *_Nullable __autoreleasing *)outError {
    NSNumber *age = *ioValue;
    if (age.integerValue == 10) {
        return YES;
    }
    return YES;
}

@end



@interface Book : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) CGFloat price;
@end
@implementation Book
@end



@interface Address : NSObject
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *district;
@property (nonatomic, strong) Person *p;
@end
@implementation Address
@end


@implementation KVCTest

+ (void)testKVC {
//    [KVCTest testKVCKeyPath];
//    [KVCTest testKVCNormalOrStructValue];
//    [KVCTest testContainer];
//    [KVCTest testOperator];
//    [KVCTest testModel];
    [KVCTest testValid];
}

+ (void)testKVCKeyPath {
    Person *person = [[Person alloc] init];
    Student *student = [[Student alloc] init];
    [person setValue:student forKey:@"student"];
    {
        //方式一
        [student setValue:@"小明" forKey:@"name"];
        NSString *name = [[person valueForKey:@"student"] valueForKey:@"name"];
        NSLog(@"name:%@",name);
    }
    {
        //方式二
        [person setValue:@"小小" forKeyPath:@"student.name"];
        NSLog(@"name:%@",[person valueForKeyPath:@"student.name"]);
    }
}

/*
 如果原本的变量类型是值类型或者结构体，返回值会封装成NSNumber或者NSValue对象。这两个类会处理从数字，布尔值到指针和结构体任何类型。然后开发者需要手动转换成原来的类型。尽管valueForKey:会自动将值类型封装成对象，但是 setValue:forKey:却不行。你必须手动将值类型转换成NSNumber或者NSValue类型，才能传递过去。
 使用NSValue的数据类型有：CGPoint、CGRect、UIEdgeInsets、UIOffset、CGAffineTransform等
 //*/
+ (void)testKVCNormalOrStructValue {
    //处理数值类型
    Person *person = [[Person alloc]init];
    [person setValue:[NSNumber numberWithInteger:20] forKey:@"age"];
    NSLog(@"age:%@", [person valueForKey:@"age"]);

    //处理结构体
    ThreeFloats floats = { 1., 2., 3. };
    NSValue *value = [NSValue valueWithBytes:&floats objCType:@encode(ThreeFloats)];
    [person setValue:value forKey:@"threeFloats"];
    NSValue *reslut = [person valueForKey:@"threeFloats"];
    NSLog(@"%@", reslut);

    ThreeFloats th;
    [reslut getValue:&th size:sizeof(ThreeFloats)];
    NSLog(@"%f - %f - %f", th.x, th.y, th.z);
}

+ (void)testContainer {
    Student *student = [[Student alloc]init];
    student.classArr = @[@"Chinese",@"Mathematics",@"English"];
    [student setValue:@"小明" forKey:@"name"];
    id a = [student mutableArrayValueForKey:@"classArr"];
    [a addObject:@"Physical"];
    id b = [student valueForKey:@"classArr"];
    // [b addObject:@"Physical"];
    id c = [student mutableArrayValueForKey:@"classArr"];// a和c地址是一样的，mutableArrayValueForKey返回的是指针，并没有生成拷贝
    NSLog(@"name:%@,class:%@",[student valueForKey:@"name"],[student valueForKey:@"classArr"]);
}

/* KVC处理集合运算符
 
 聚合操作符

 @avg: 返回集合中指定对象属性的平均值
 @count: 返回集合中指定对象属性的个数
 @max: 返回集合中指定对象属性的最大值
 @min: 返回集合中指定对象属性的最小值
 @sum: 返回集合中指定对象属性值之和

 数组操作符

 @distinctUnionOfObjects: 返回集合中指定对象属性的集合，且会进行去重操作
 @unionOfObjects: 返回集合中指定对象属性的集合，并不会删除相同元素。

 嵌套操作符

 @distinctUnionOfArrays: 返回指定的属性相对应的所有集合的组合的不同对象集合，并会删除相同的元素
 @unionOfArrays: 返回指定的属性相对应的所有集合的组合的不同对象集合，但是不会删除相同元素
 @distinctUnionOfSets: 返回指定的属性相对应的所有集合的组合中的不同对象集合，并删除相同元素，返回的是 NSSet
 //*/

+ (void)testOperator {
    Book *book1 = [Book new];
    book1.name = @"编程珠玑";
    book1.price = 50;
    Book *book2 = [Book new];
    book2.name = @"Java编程思想";
    book2.price = 20;
    Book *book3 = [Book new];
    book3.name = @"漫画算法";
    book3.price = 30;

    Book *book4 = [Book new];
    book4.name = @"算法图解";
    book4.price = 30;

    NSArray *arrBooks = @[book1, book2, book3, book4];
    //计算总价
    NSNumber *sum = [arrBooks valueForKeyPath:@"@sum.price"];
    NSLog(@"sum:%f", sum.floatValue);
    //计算平均价格
    NSNumber *avg = [arrBooks valueForKeyPath:@"@avg.price"];
    NSLog(@"avg:%f", avg.floatValue);
    //计算书本数量
    NSNumber *count = [arrBooks valueForKeyPath:@"@count"];
    NSLog(@"count:%f", count.floatValue);
    //计算最小的书本价格
    NSNumber *min = [arrBooks valueForKeyPath:@"@min.price"];
    NSLog(@"min:%f", min.floatValue);
    //计算最大的书本价格
    NSNumber *max = [arrBooks valueForKeyPath:@"@max.price"];
    NSLog(@"max:%f", max.floatValue);
    //返回书本的价格集合
    NSArray *distinctPrice = [arrBooks valueForKeyPath:@"@distinctUnionOfObjects.price"];
    NSLog(@"distinctPrice:%@", distinctPrice);
    //返回书本的价格集合
    NSArray *unionPrice = [arrBooks valueForKeyPath:@"@unionOfObjects.price"];
    NSLog(@"unionPrice:%@", unionPrice);
    
    NSArray *arr1 = @[book1,book2];
    NSArray *arr2 = @[book3,book4];
    NSArray *arr = @[arr1,arr2];
    NSArray *collectedDistinctPrice = [arr valueForKeyPath:@"@distinctUnionOfArrays.price"];
    NSLog(@"collectedDistinctPrice:%@", collectedDistinctPrice);
    NSArray *collectedPrice = [arr valueForKeyPath:@"@unionOfArrays.price"];
    NSLog(@"collectedPrice:%@", collectedPrice);
}

+ (void)testModel {
    //模型转字典
    Address *address = [Address new];
    address.country = @"China";
    address.province = @"Guang Dong";
    address.city = @"Shen Zhen";
    address.district = @"Nan Shan";
    address.p = [[Person alloc] init];
    address.p.age = 18;
    Student *student = [[Student alloc]init];
    student.classArr = @[@"Chinese",@"Mathematics",@"English"];
    [student setValue:@"小明" forKey:@"name"];
    [address.p setValue:student forKey:@"student"];
    ThreeFloats floats = { 1., 2., 3. };
    NSValue *value = [NSValue valueWithBytes:&floats objCType:@encode(ThreeFloats)];
    [address.p setValue:value forKey:@"threeFloats"];
    NSArray *arr = @[@"country", @"province", @"city", @"district", @"p"];
    NSDictionary *dict = [address dictionaryWithValuesForKeys:arr];
    NSDictionary *dict1 = [address dictionaryWithValuesForKeys:arr];
    
    NSLog(@"person：%@", address.p);
    NSLog(@"person：%@", [dict objectForKey:@"p"]);
    NSLog(@"person：%@", [dict1 objectForKey:@"p"]);//dict与dict1里的person实际都指向了address的person
    NSLog(@"district：%p", address.district);
    NSLog(@"district：%p", [dict objectForKey:@"district"]);
    NSLog(@"district：%p", [dict1 objectForKey:@"district"]);
    [address.p setValue:@"小小" forKeyPath:@"student.name"];
    [address setValue:@"Nan Shan888" forKey:@"district"];
    NSLog(@"%@", dict);
    NSLog(@"person after change value value in address：%@", address.p);
    NSLog(@"person after change value value in dict：%@", [dict objectForKey:@"p"]);
    NSLog(@"district after change value in address：%p", address.district);
    NSLog(@"district after change value in dict：%p", [dict objectForKey:@"district"]);
    // 值类型和基础类型

    //字典转模型
    NSDictionary *modifyDict = @{ @"country": @"China", @"province": @"Guang Dong", @"city": @" Shen Zhen", @"district": @"Nan Shan" };
    [address setValuesForKeysWithDictionary:modifyDict];            //用key Value来修改Model的属性
    NSLog(@"country:%@  province:%@ city:%@ district:%@", address.country, address.province, address.city, address.district);
}

+ (void)testValid {
    Person *person = [[Person alloc] init];
    NSNumber *age = @10;
    NSError *error;
    NSString *key = @"age";
    BOOL isValid = [person validateValue:&age forKey:key error:&error];
    if (isValid) {
        NSLog(@"键值匹配");
        [person setValue:age forKey:key];
    } else {
        NSLog(@"键值不匹配");
    }

    NSLog(@"age:%@", [person valueForKey:@"age"]);
}

@end
