//
//  StaticTest.h
//  test
//
//  Created by 欧小强 on 2022/10/19.
//  Copyright © 2022 APPL. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StaticTest : NSObject

+ (void)test;

@end

NS_ASSUME_NONNULL_END
